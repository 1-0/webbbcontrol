#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import render_template, redirect, request, render_template_string, jsonify, Response
import ConfigParser, os, json
import Settings, configparser
from config import create_app
from utils import *
from forms import FasSettingsForm
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter
import client_mqtt


#~ COMMAND_STRING = u'''python ./BlackBeanControl.py -c '''

app = create_app()

@app.route('/', methods=('GET', 'POST'))
@app.route('/sendstate', methods=('GET', 'POST'))
def submit():
    form = FasSettingsForm()
    #~ last_command = None
    if request.method == 'POST':
        catchCommand(json.loads(request.data))

        # ~ print(request.remote_addr)
        # ~ print (request.data)
        return 'ok'

    return render_template('main_extended.html',
                           form=form, 
                           last_command=request.data)


@app.route('/site2', methods=('GET', 'POST'))
def submit2():
    form = FasSettingsForm()
    last_command = None
    if request.method == 'POST':
        #~ print(request.form)
        if form.validate_on_submit():
            catchCommand2(request.form)
            last_command = request.form['device'] + ': ' + \
                           str(request.form['temperature']) + u'°C - ' + \
                           request.form['command']

    return render_template('main_simply.html',
                           form=form,
                           last_command=last_command)


@app.route('/metrics/<area_name>/<point_id>', methods=('GET', 'POST'))
@app.route('/metrics/<area_name>/<point_id>.json', methods=('GET', 'POST'))
def get_metrics(area_name, point_id):
    """get_metrics(point_id) - return methrics dataset from az-sensor"""
    if area_name not in ['az', ]:
        return not_found(404)
    if point_id == "mqtt":
        response_dict = client_mqtt.get_mqtt()
        # ~ # debug mokup
        # ~ response_dict = {"CO2": 777, "Connected": "ON", "Humidity": 52, "Temp": 22, "Uptime": 5}
    elif point_id in ['temperature', 'humidity', 'co2']:
        time_duration = request.args.get('time_duration')
        time_group = request.args.get('time_group')
        response_dict = {}
        response_dict = get_history_points(res_dict=response_dict,
                                           point_id=point_id,
                                           time_duration=time_duration,
                                           area_name=area_name,
                                           time_group=time_group,
                                           )
        # ~ # debug mokup
        # ~ import random
        # ~ a = 4
        # ~ if point_id == 'humidity':
            # ~ a = 50
        # ~ elif point_id == 'co2':
            # ~ a = 1000
        # ~ response_dict = {"2018-06-08 12:20:52": 27 + random.randint (0, a),
                         # ~ "2018-06-08 12:21:12": 21 + random.randint (0, a),
                         # ~ "2018-06-08 12:21:22": 22 + random.randint (0, a),
                         # ~ "2018-06-08 12:21:32": 25 + random.randint (0, a),
                         # ~ "2018-06-08 12:22:44": 24.5 + random.randint (0, a),
                         # ~ "2018-06-08 12:23:16": 29 + random.randint (0, a),
                         # ~ "2018-06-08 12:24:22": 21.5 + random.randint (0, a),
                        # ~ }
    else:
        return not_found(404)
    return jsonify(response_dict)


@app.route('/metrics_editor', methods=('GET', 'POST'))
def metrics_edit():
    metrics_id = request.args.get('metrics_id')
    return render_template('metrics.edit.html', metrics_id=metrics_id)


# HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5551, debug=True)

