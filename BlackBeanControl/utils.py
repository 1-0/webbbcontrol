﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from black_bean_control import SendingCommand
from influxdb import InfluxDBClient
from flask import render_template



def catchCommand(commands):
    command = [int(commands['temperature']), ]
    if commands['onoff']:
        command.append('onoff')
    elif commands['cool']:
        command.append('cool')
    elif commands['heat']:
        command.append('heat')
    elif commands['set']:
        command.append('set')

    if commands['samurai']:
        SendingCommand(command+['samurai',])
        time.sleep(0.15)
    if commands['arvin']:
        SendingCommand(command+['arvin',])
        time.sleep(0.15)


def catchCommand2(commands):
    command = [int(commands['temperature']), ]
    command.append(commands['command'])
    #~ print(commands['device'])
    if 'samurai' in commands['device']:
        SendingCommand(command+['samurai',])
        time.sleep(0.15)
    if 'arvin' in commands['device']:
        SendingCommand(command+['arvin',])
        time.sleep(0.15)


def writeHistory(event):
    log = open("commands.log", "a")
    string = time.strftime("[%a, %d %b %Y %H:%M:%S]: ", time.gmtime()) + event
    res = log.write(string);
    log.close()
    return res


def readHistory(event):
    log = open("commands.log", "r+")
    string = time.strftime("[%a, %d %b %Y %H:%M:%S]: ", time.gmtime()) + event
    log.write(string);
    log.close()

def get_history_points(res_dict, 
                       point_id="co2",
                       time_duration="360m",
                       area_name="az",
                       time_group="5m",
                       db_name="office",
                      ):
    
    res_dict = {}
    client = InfluxDBClient(host='18.0.0.11', port=8086)
    client.switch_database(db_name)
    if time_group=='0m':
        time_group = None
    query_str = render_template('history_points.sql', 
                                point_id=point_id,
                                time_duration=time_duration,
                                area_name=area_name,
                                time_group=time_group
                                )
    # ~ print(query_str)
    res = client.query(query_str)
    # ~ print(res.raw)
    points = res.get_points()
    for point in points:
        if time_group:
            res_dict[point['time']] = point['mean']
        else:
            res_dict[point['time']] = point[point_id]
    return res_dict


