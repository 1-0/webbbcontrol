#!/usr/bin/env python2

import paho.mqtt.client as mqtt
import time
from copy import copy
import time
import tel

LAST_TELEGRAM = []
dict_msg = {}


def on_connect(client, userdata, flags, rc):
    """on_connect(client, userdata, flags, rc) - The callback for when the
    client receives a CONNACK response from the server."""
    # print("Connected with result code "+str(rc))
    global dict_msg

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")


def on_message(client, userdata, msg):
    """on_message(client, userdata, msg) - The callback for when a PUBLISH
    message is received from the server."""
    global dict_msg
    topic = copy(msg.topic[4:])
    payload = copy(msg.payload.decode('utf-8'))
    dict_msg[topic] = payload
    # print(msg.topic+" "+str(msg.payload))


def get_mqtt():
    """get_mqtt() - get data from mqtt-server"""
    global dict_msg
    global LAST_TELEGRAM
    client = mqtt.Client("CO2")
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect("18.0.0.11", 1883, 60)
    client.loop_start()
    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.

    time.sleep(15)
    # print(str(dict_msg))
    client.loop_stop()
    ttt = time.gmtime()
    if (
        "CO2" in dict_msg and
        int(dict_msg["CO2"]) > 1100 and
        ttt.tm_wday > 0 and
        ttt.tm_wday < 6 and
        (
         ttt.tm_min == 0 or
         ttt.tm_min == 15 or
         ttt.tm_min == 30 or
         ttt.tm_min == 45
        ) and
        ttt.tm_sec < 30 and
        ttt.tm_hour > 7 and
        ttt.tm_hour < 16 and
        LAST_TELEGRAM != [
                          ttt.tm_year,
                          ttt.tm_mon,
                          ttt.tm_mday,
                          ttt.tm_hour,
                          ttt.tm_min
                         ]
       ):

            LAST_TELEGRAM = [
                             ttt.tm_year,
                             ttt.tm_mon,
                             ttt.tm_mday,
                             ttt.tm_hour,
                             ttt.tm_min
                            ]
            tel.send_telegram(list_msg=['WARNING! In Lot IT-room level CO2 = ' +
                              str(dict_msg["CO2"]), ])
    return dict_msg
    # client.loop_forever()


if __name__ == "__main__":
    print(str(get_mqtt()))
