﻿var intervals_arr = [];

function handleShape(e) {
    $(".shape")
        .removeClass("circle pill square rectangle")
        .addClass($(e.target).val());
};

function handleToggle(e) {
    var target = $(e.target);

    if (target.is(".brand-toggle")) {
        var checked = target.is(":checked"),
            value = $("[name='brand']")
            .filter(":checked")
            .attr("data-" + target[0].id)
        $(".shape").css(target[0].id, checked ? value : "");
    } else {
        $(".shape").toggleClass(target[0].id, target.is(":checked"));
    }
}

function updateBrand() {
    handleShape({
        target: $("[name='shape']:checked")
    });
    $(".toggle:checked").each(function() {
        handleToggle({
            target: $(this)
        });
    });
}

function getName(c_str) {
    if (c_str == "SAMURAI") {
        return "samurai";
    } else if (c_str == "ARVIN") {
        return "arvin";
        //~ } else if (c_str=="radio-1") {
        //~ return "set";
    } else if (c_str == "radio-2") {
        return "onoff";
    } else if (c_str == "radio-3") {
        return "heat";
    } else if (c_str == "radio-4") {
        return "cool";
    }
    return c_str
}

function setName(c_str) {
    if (c_str == "samurai") {
        return "SAMURAI";
    } else if (c_str == "arvin") {
        return "ARVIN";
        //~ } else if (c_str=="set") {
        //~ return "radio-1";
    } else if (c_str == "onoff") {
        return "radio-2";
    } else if (c_str == "heat") {
        return "radio-3";
    } else if (c_str == "cool") {
        return "radio-4";
    }
    return c_str
}

function collectSetings() {
    var inputs = $(".check");
    var msg = {};
    for (var i = 0; i < inputs.length; i++) {
        c_id = getName($(inputs[i]).attr('id'));
        c_state = $(inputs[i]).prop('checked');
        msg[c_id] = c_state;
    }
    if (!((msg["samurai"]) || (msg["arvin"]))) {
        $("span.ui-dialog-title").text("Задайте кондиционер!");
        $("#dialog_massage").html("Следует выбрать хотя бы один кондиционер");
        $("#dialog").dialog("open");
        return -1;
    }
    c_temp = $($("#custom-handle")[0]).html();
    msg["temperature"] = c_temp;
    return msg;
}

function handleSaveCookie() {
    var settings = collectSetings();
    if (settings != -1) {
        Cookies.set('settings_lot_cond', settings, {
            expires: 1010,
            path: ''
        });
        $("span.ui-dialog-title").text("Сохранение настроек");
        $("#dialog_massage").html("Настройки сохранены локально в cookie");
        $("#dialog").dialog("open");
    }
    //~ console.log("width=" + $( "#Metric-az-temperature" ).width() + " height=" + $( "#Metric-az-temperature" ).height());
    //~ var settings_metrics = {"area_name": "az", };
    //~ var settings_metrics = default_metrics;
    //~ load_metrics();
    //~ Cookies.set('settings_lot_metrics', settings_metrics, {
        //~ expires: 1010,
        //~ path: ''
    //~ });
}

function handleLoadCookie() {
    var handle = $("#custom-handle");
    var slider = $("#slider");
    var inputs = $(".check");
    var saved = Cookies.get('settings_lot_cond');
    if (!(saved === undefined || saved === null)) {
        var settings = JSON.parse(Cookies.get('settings_lot_cond'));
        //~ console.log(settings);
        c_temp = settings['temperature'];
        $("#amount").val(c_temp + "°C");
        slider.slider("value", c_temp);
        handle.text(c_temp);
        delete settings['temperature'];
        for (property in settings) {
            if (settings[property]) {
                var c_name = "#" + setName(property);
                var c_tag = $(c_name);
                c_tag.prop("checked", true);
            } else {
                var c_name = "#" + setName(property);
                var c_tag = $(c_name);
                c_tag.prop("checked", false);
            }
        }
        $(".check").checkboxradio("refresh");
    }
}

function handleSend() {
    var msg = collectSetings();
    if (msg != -1) {
        //~ console.log(msg);
        handleSaveCookie()
        $.ajax({
            url: '/sendstate',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(msg),
            dataType: 'json'
        });
    }
}

function get_mqtt_state() {
    $("#mqtt_h3").html('Значения показателей <span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span>');
    $.getJSON("/metrics/az/mqtt.json", function(co2data) {
        $("#mqtt_CO2").text("Уровень CO2: " + co2data["CO2"] + " ppm");
        $("#mqtt_Connected").text("Состояние подключения: " + co2data["Connected"]);
        if (co2data["Connected"] != "ON") {
            $("#mqtt_Connected").css('background', 'red')
        } else {
            $("#mqtt_Connected").css('background', 'green')
        }
        $("#mqtt_Humidity").text("Влажность: " + co2data["Humidity"] + " %");
        $("#mqtt_Temp").text("Температура: " + co2data["Temp"] + ' °C');
        $("#mqtt_Uptime").text("Время непрерывной работы: " + co2data["Uptime"]);
        $("#mqtt_h3").html('Значения показателей');
    });
}

function load_metrics(){
    var saved = Cookies.get('settings_lot_metrics');
    if (!(saved === undefined || saved === null)) {
        var get_settings = JSON.parse(Cookies.get('settings_lot_metrics'));
    } else {
        var get_settings = default_metrics;
        Cookies.set('settings_lot_metrics', get_settings, {
            expires: 1010,
            path: ''
        });
    }
    for (metrics_id in get_settings["metrics_settings"]) {
        s_elem = get_settings["metrics_settings"][metrics_id];
        editor_span =  '<span>'+
                          '<a href="/metrics_editor?metrics_id='+metrics_id+'">'+
                             '<span '+
                                  'class="glyphicon glyphicon-cog"'+
                                  'aria-hidden="true">'+
                             '</span>'+
                          '</a>'+
                    '<span>';
        m_header = $('<h4 id="m-head-'+metrics_id+'">'+s_elem["metrics_header"]+' '+editor_span+'</h4>');
        m_header.appendTo( '#mqtt_info' );
        id_canvas = 'Metric-canvas-'+metrics_id;
        //~ $('#mqtt_info_'+id_canvas).append('<canvas id="'+id_canvas+'"><canvas>'
        m_div = $('<div id="mqtt_info_'+id_canvas+'"></div>');
        m_div.appendTo( '#mqtt_info' );
        m_canvas = $('<canvas id="'+id_canvas+'">');
        m_canvas.appendTo( "#mqtt_info_"+id_canvas );
        chartMqtt(s_elem["point_id"],
                  s_elem["metrics_label"],
                  s_elem["metrics_color"],
                  s_elem["area_name"],
                  s_elem["time_duration"],
                  s_elem["time_group"],
                  id_canvas
                  );
        intervals_arr.push(setInterval(
                              chartMqtt, 
                              60000,
                              s_elem["point_id"],
                              s_elem["metrics_label"],
                              s_elem["metrics_color"],
                              s_elem["area_name"], 
                              s_elem["time_duration"],
                              s_elem["time_group"],
                              id_canvas
                                      )
                           );
    }
    new_editor_span =  ' <span>'+
                      '<a href="/metrics_editor?metrics_id='+(parseInt(metrics_id)+1)+'">'+
                         '<span '+
                              'class="glyphicon glyphicon-plus"'+
                              'aria-hidden="true">'+
                         '</span>'+
                      '</a>'+
                '<span>';
    $("#mqtt_info_h3").html($("#mqtt_info_h3").html()+new_editor_span);
}

// var func = location.reload(),
//     delay = 36000000;
// setTimeout(func, delay);

function reload_page()
{
    window.location.reload(true);
}

$(function() {
    moment.locale('ru');
    var handle = $("#custom-handle");
    var spinner = $("#spinner").spinner();
    spinner.spinner("enable");
    spinner.spinner("value", 25);
    handle.text(25);
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });
    $("#slider").slider({
        value: 25,
        min: 20,
        max: 30,
        step: 1,
        slide: function(event, ui) {
            $("#amount").val(ui.value + "°C");
            spinner.spinner("value", ui.value);
            handle.text(ui.value);
        }
    });
    $("#amount").val($("#slider").slider("value") + "°C");

    // Initalize widgets
    $("input").checkboxradio();
    $(".shape-bar, .brand").controlgroup();
    $(".toggles").controlgroup({
        direction: "vertical"
    });

    // Bind event handlers
    $("#send").on("click", handleSend);
    $("#save_cookie").on("click", handleSaveCookie);
    $("#load_cookie").on("click", handleLoadCookie);

    // Set initial values
    updateBrand();
    handleLoadCookie();
    //~ $(".tron_info").show(500);

    // Mqtt data load
    get_mqtt_state();
    //~ $("#mqtt_info").show(150);
    reload_mqtt = setInterval(get_mqtt_state, 15000);
    load_metrics();
    setInterval(function(){ reload_page(); },60*60000);
});
