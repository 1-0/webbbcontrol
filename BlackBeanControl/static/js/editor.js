﻿var id_canvas = '';
var get_settings = {};
var intervals_arr = [];

function load_metrics_settings(){
    var saved = Cookies.get('settings_lot_metrics');
    if (!(saved === undefined || saved === null)) {
        get_settings = JSON.parse(Cookies.get('settings_lot_metrics'));
    } else {
        get_settings = default_metrics;
    }
    if (metrics_id < get_settings["metrics_settings"].length) {
        var s_elem = get_settings["metrics_settings"][metrics_id];
    } else {
        var s_elem = default_metrics["metrics_settings"][0];
    }
    id_canvas = 'Metric-'+metrics_id;
    m_div = $('<div id="mqtt_info_'+id_canvas+'"></div>');
    m_div.appendTo( '#mqtt_info' );
    m_canvas = $('<canvas id="'+id_canvas+'">');
    m_canvas.appendTo( "#mqtt_info_"+id_canvas );
    //~ var m_canvas = $('<canvas id="'+id_canvas+'">');
    //~ m_canvas.appendTo( '#mqtt_info' );
    $( '#Metric-header' ).html(s_elem["metrics_header"]);
    $( '#time_duration' ).val(s_elem["time_duration"].substr(0, s_elem["time_duration"].length-1));
    $( '#metrics_header' ).val(s_elem["metrics_header"]);
    $( '#point_id option[value='+ s_elem["point_id"] + ']' ).prop('selected', true);
    $( '#metrics_label' ).val(s_elem["metrics_label"]);
    color_id = "#" + s_elem["metrics_color"][0].toString(16) +
    s_elem["metrics_color"][1].toString(16) +
    s_elem["metrics_color"][2].toString(16);
    $( '#metrics_color' ).val(color_id);
    $( '#time_group' ).val(s_elem["time_group"].substr(0, s_elem["time_group"].length-1));
    $( '#area_name' ).val(s_elem["area_name"]);
    $( '#db_name' ).val(s_elem["db_name"]);
    chartMqtt(s_elem["point_id"],
              s_elem["metrics_label"],
              s_elem["metrics_color"],
              s_elem["area_name"],
              s_elem["time_duration"],
              s_elem["time_group"],
              id_canvas
              );
    intervals_arr.push(setInterval(
                          chartMqtt,
                          60000,
                          s_elem["point_id"],
                          s_elem["metrics_label"],
                          s_elem["metrics_color"],
                          s_elem["area_name"], 
                          s_elem["time_duration"],
                          s_elem["time_group"],
                          id_canvas
                                )
                          );
    //~ console.log(intervals_arr);
}

function save_metrics_settings (){
    var saved = Cookies.get('settings_lot_metrics');
    if (!(saved === undefined || saved === null)) {
        get_settings = JSON.parse(Cookies.get('settings_lot_metrics'));
    } else {
        get_settings = default_metrics;
    }
    // collect settings
    var s_elem = {};
    s_elem["time_duration"] = $( '#time_duration' ).val() + 'm';
    s_elem["metrics_header"] = $( '#metrics_header' ).val();
    s_elem["point_id"] = $("#point_id option").filter(":selected").val();
    s_elem["metrics_label"] = $( '#metrics_label' ).val();
    var color_name = $( '#metrics_color' ).val();
    s_elem["metrics_color"] = [
                               parseInt(color_name.substr(1, 2), 16),
                               parseInt(color_name.substr(3, 2), 16),
                               parseInt(color_name.substr(5, 2), 16),
                               ];
    
    //~ console.log(s_elem["metrics_color"]);
    
    s_elem["time_group"] = $( '#time_group' ).val() + 'm';
    s_elem["area_name"] = $( '#area_name' ).val();
    s_elem["db_name"] = $( '#db_name' ).val();
    
    // save settings
    if (metrics_id <= get_settings["metrics_settings"].length) {
        get_settings["metrics_settings"][metrics_id] = s_elem;
    } else {
        get_settings["metrics_settings"].push(s_elem);
    }
    
    //~ console.log(get_settings);
    
    Cookies.set('settings_lot_metrics', get_settings, {
        expires: 1010,
        path: ''
    });
    
    window.location = '/'
}

function remove_settings() {
    get_settings = JSON.parse(Cookies.get('settings_lot_metrics'));
    get_settings["metrics_settings"].splice(metrics_id, 1);
    Cookies.set('settings_lot_metrics', get_settings, {
        expires: 1010,
        path: ''
    });
    window.location = '/'
}


$(function() {
    moment.locale('ru');

    // Bind event handlers
    $("#save_settings").on("click", save_metrics_settings);
    $("#reload_settings").on("click", load_metrics_settings);
    $("#remove_settings").on("click", remove_settings);


    // Mqtt data load
    load_metrics_settings();
});
