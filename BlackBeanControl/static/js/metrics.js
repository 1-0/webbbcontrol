﻿var my_charts = {};
var default_metrics = {'metrics_settings': [
    {
    'point_id': 'temperature',
    'time_duration': '360m',
    'time_group': '5m',
    'metrics_header': 'Температура',
    'metrics_label': ' °C',
    'metrics_color': [255, 25, 255],
    'area_name': 'az',
    'db_name': 'office',
    },
    {
    'point_id': 'humidity',
    'time_duration': '360m',
    'time_group': '5m',
    'metrics_header': 'Влажность',
    'metrics_label': ' %',
    'metrics_color': [255, 255, 25],
    'area_name': 'az',
    'db_name': 'office',
    },
    {
    'point_id': 'co2',
    'time_duration': '360m',
    'time_group': '5m',
    'metrics_header': 'Уровень CO2',
    'metrics_label': ' ppm',
    'metrics_color': [25, 255, 255],
    'area_name': 'az',
    'db_name': 'office',
    },
]};

function chart_data(data_taked) {
    var c_data = []
    for (key in data_taked) {
        c_data.push({
            x: new Date(key),
            y: data_taked[key]
        });
    }
    return c_data
}

function getMaxNorm(point_id, points_data) {
    var max_val;
    var max_arr = [];
    var min_x = points_data[0].x;
    var max_x = points_data[0].x;
    switch(point_id) {
        case 'temperature':
            max_val = 27;
            break;
        case 'humidity':
            max_val = 55;
            break;
        case 'co2':
            max_val = 900;
            break;
        default:
            max_val = '';
            break;
        return max_val;
    }
    if (max_val != '') {
        for (key in points_data) {
            if (min_x > points_data[key].x) {
                min_x = points_data[key].x;
            } else if (max_x < points_data[key].x) {
                max_x = points_data[key].x;
            }
        };
        max_arr.push({
            x: min_x, 
            y: max_val
        });
        max_arr.push({
            x: max_x, 
            y: max_val
        });
        //~ console.log(max_arr);
        return max_arr;
    }
}

function chartMqtt(point_id,
                   label_text,
                   rgb_c, 
                   area_name, 
                   time_duration,
                   time_group,
                   id_canvas
                   ) {
    
    var list_cog = $(".glyphicon-cog");
    list_cog.removeClass("glyphicon-cog");
    list_cog.addClass("glyphicon-time");
    
    //~ document.getElementById('mqtt_info').innerHTML = '';
    //~ console.log(document.getElementById('mqtt_info').innerHTML);
    $(id_canvas).remove(); // this is my <canvas> element
    $('#mqtt_info_'+id_canvas).html('');
    $('#mqtt_info_'+id_canvas).append('<canvas id="'+id_canvas+'"><canvas>');
    //~ canvas = document.querySelector('#results-graph'); // why use jQuery?
    //~ ctx = canvas.getContext('2d');
    var source_json = "/metrics/" +
                      area_name + "/" +
                      point_id +
                      ".json?time_duration=" +
                      time_duration +
                      "&time_group=" +
                      time_group;
    $.getJSON(source_json, function(data_getted) {
        var data_list = chart_data(data_getted);
        var ctx_mqtt = document.getElementById(id_canvas).getContext('2d');
        var mqttLineChart = new Chart(ctx_mqtt, {
            type: 'line',
            data: {
                datasets: [{
                        label: label_text,
                        data: data_list,
                        backgroundColor: 'rgba(' +
                                         rgb_c[0] +
                                         ', ' +
                                         rgb_c[1] +
                                         ', ' +
                                         rgb_c[2] +
                                         ', 0.2)',
                        borderColor: 'rgba(' +
                                     rgb_c[0] +
                                     ', ' +
                                     rgb_c[1] +
                                     ', ' +
                                     rgb_c[2] +
                                     ', 1)',
                        }, 
                        {
                        label: 'MAX '+ label_text,
                        data: getMaxNorm(point_id, data_list),
                        backgroundColor: 'rgba(1, 1, 1, 0.1)',
                        borderColor: 'rgba(1, 1, 1, 0.1)',
                        //~ borderColor: 'rgba(' +
                                         //~ rgb_c[0] +
                                         //~ ', ' +
                                         //~ rgb_c[1] +
                                         //~ ', ' +
                                         //~ rgb_c[2] +
                                         //~ ', 0.7)',
                        }
                    ],},

            options: {
                animation: {
                    duration: 500
                    },
                scales: {
                    xAxes: [{
                        type: 'time',
                    }],
                }
            }
        });
    });
    list_cog.removeClass("glyphicon-time");
    list_cog.addClass("glyphicon-cog");
    
}
