﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms import SubmitField, RadioField, SelectMultipleField, IntegerField
from wtforms.validators import DataRequired


DEVICES_LIST = [(u'samurai', u'SAMURAI'), \
        (u'arvin', u'ARVIN'), \
        ]
COMMANDS_LIST = [(u'onoff', u'Выключить/[включить для SAMURAI]'), \
        #~ (u'set', u'Задать температуру'), \
        (u'heat', u'Нагрев/[включить для ARVIN]'), \
        (u'cool', u'Охлаждение/[включить для ARVIN]'), \
        ]


class FasSettingsForm(FlaskForm):
    device=RadioField(label=u"Выберите устройство: ", choices=DEVICES_LIST, validators=[DataRequired()])
    command=RadioField(label=u"Выберите команду: ", choices=COMMANDS_LIST, validators=[DataRequired()])
    temperature=IntegerField(label=u"Температура: ", validators=[DataRequired()],
    #~ options=' min="20" max="30" step="1" value ="25" '
    )
    send = SubmitField(label=u"Установить")


