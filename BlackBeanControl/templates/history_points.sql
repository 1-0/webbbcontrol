SELECT 
{% if time_group %}
MEAN("{{ point_id }}") 
{% else %}
"{{ point_id }}"
{% endif %}
 FROM "mes" 
WHERE 
{% if area_name %}
    area='{{ area_name }}' 
    AND 
{% endif %}
time > now()-{{ time_duration }} 
{% if time_group %}
    GROUP BY time({{ time_group }}) 
    fill(null)
{% endif %}
;
