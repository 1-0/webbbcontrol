#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''CreateCommands.py - create commands to send different air conditions'''

import unittest


def sint(s):
    '''sint(s) - convert short hex string to int'''

    new_int = int('0x' + s, 16)
    return new_int


def CreateCommandSamurai(temperature=22, action="cool"):
    '''CreateCommandSamurai(temperature=25, action="cool") - create command to
    send action and temperature to device Samurai'''

    if action == "onoff":
        new_command = 'c081'
    elif action == "cool":
        new_command = 'c001'
    elif action == "set":
        new_command = 'c013'
    elif action == "heat":
        new_command = 'c014'

    new_command += hex(temperature)[2:]
    sum_command = hex(255-sint(hex(
                    (sint(new_command[:2]) +
                     sint(new_command[2:4]) +
                     sint(new_command[4:])))[-2:]))[2:]
    new_command += str(sum_command)
    return new_command


def CreateCommandAvrin(temperature=22, action="cool"):
    '''CreateCommandSamurai(temperature=25, action="cool") - create command to
    send action and temperature to device ARVIN'''

    temper_list = [0, 8, 12, 4, 6, 14, 10, 2, 3, 11, 9, 1, 5, 13]
    temper = temper_list[temperature - 17]  # short temperature in protocol

    if action == "onoff":
        new_command = '4db2de2107f8'
        return new_command
    elif action == "cool":
        new_command = '4db2fd020'
    elif action == "set":
        new_command = '4db2fd020'
    elif action == "heat":
        new_command = '4db2fd023'

    new_command += hex(temper)[2:]
    sum_command = hex(255-sint(new_command[-2:]))[2:]
    new_command += str(sum_command)
    return new_command


class TestCommandsSamurai(unittest.TestCase):

    def test_onoff(self):
        self.assertEqual(CreateCommandSamurai(temperature=25, action="onoff"),
                         'c08119a5')
        self.assertEqual(CreateCommandSamurai(temperature=28, action="onoff"),
                         'c0811ca2')

    def test_heattemp(self):
        self.assertEqual(CreateCommandSamurai(temperature=25, action="heat"),
                         'c0141912')

    def test_vent(self):
        self.assertEqual(CreateCommandSamurai(temperature=25, action="vent"),
                         'c0131913')

    def test_cooltemp(self):
        self.assertEqual(CreateCommandSamurai(temperature=23, action="cool"),
                         'c0011727')
        self.assertEqual(CreateCommandSamurai(temperature=25, action="cool"),
                         'c0011925')
        self.assertEqual(CreateCommandSamurai(temperature=26, action="cool"),
                         'c0011a24')


class TestCommandsAvrin(unittest.TestCase):

    def test_onoff(self):
        self.assertEqual(CreateCommandAvrin(temperature=25, action="onoff"),
                         '4db2de2107f8')

    def test_heattemp(self):
        self.assertEqual(CreateCommandAvrin(temperature=29, action="heat"),
                         '4db2fd0235ca')
        self.assertEqual(CreateCommandAvrin(temperature=30, action="heat"),
                         '4db2fd023dc2')

    def test_cooltemp(self):
        self.assertEqual(CreateCommandAvrin(temperature=17, action="cool"),
                         '4db2fd0200ff')
        self.assertEqual(CreateCommandAvrin(temperature=23, action="cool"),
                         '4db2fd020af5')
        self.assertEqual(CreateCommandAvrin(temperature=25, action="cool"),
                         '4db2fd0203fc')
        self.assertEqual(CreateCommandAvrin(temperature=30, action="cool"),
                         '4db2fd020df2')


class TestSint(unittest.TestCase):

    def test_convert(self):
        self.assertEqual(sint('c0'), 192)
        self.assertEqual(sint('01'), 1)
        self.assertEqual(sint('81'), 129)


if __name__ == '__main__':
    unittest.main()
