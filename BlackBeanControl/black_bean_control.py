#!/usr/bin/env python
# -*- coding: utf-8 -*-

import broadlink, configparser
import sys, getopt
import time, binascii
import netaddr
import Settings
from os import path
from Crypto.Cipher import AES
from CreateCommands import CreateCommandSamurai
from CreateCommands import CreateCommandAvrin



def SendingCommand(SentCommands = [25, 'set', 'samurai']):
    #~ print(SentCommands)
    SentCommand = SentCommands[1]
    SettingsFile = configparser.ConfigParser()
    SettingsFile.optionxform = str
    SettingsFile.read(Settings.BlackBeanControlSettings)

    #~ SentCommand = ''
    ReKeyCommand = False
    DeviceName=''
    DeviceIPAddress = ''
    DevicePort = ''
    DeviceMACAddres = ''
    DeviceTimeout = ''
    AlternativeIPAddress = ''
    AlternativePort = ''
    AlternativeMACAddress = ''
    AlternativeTimeout = ''


    RealIPAddress = Settings.IPAddress


    RealPort = Settings.Port
    RealPort = int(RealPort.strip())
    RealMACAddress = Settings.MACAddress
    RealMACAddress = netaddr.EUI(RealMACAddress)
    RealTimeout = Settings.Timeout
    RealTimeout = int(RealTimeout.strip())
    RM3Device = broadlink.rm((RealIPAddress, RealPort), RealMACAddress)
    RM3Device.auth()

    if ReKeyCommand:
        if SettingsFile.has_option('Commands', SentCommand):
            CommandFromSettings = SettingsFile.get('Commands', SentCommand)

            if CommandFromSettings[0:4] != '2600':
                RM3Key = RM3Device.key
                RM3IV = RM3Device.iv

                DecodedCommand = binascii.unhexlify(CommandFromSettings)
                AESEncryption = AES.new(str(RM3Key), AES.MODE_CBC, str(RM3IV))
                EncodedCommand = AESEncryption.encrypt(str(DecodedCommand))
                FinalCommand = EncodedCommand[0x04:]
                EncodedCommand = FinalCommand.encode('hex')

                if 'samurai' in SentCommands:
                    BlackBeanControlIniFile = open(path.join(Settings.ApplicationDir, 'BlackBeanControl.ini'), 'w')
                elif 'arvin' in SentCommands:
                    BlackBeanControlIniFile = open(path.join(Settings.ApplicationDir, 'BlackBeanControl2.ini'), 'w')

                SettingsFile.set('Commands', SentCommand, EncodedCommand)
                SettingsFile.write(BlackBeanControlIniFile)
                BlackBeanControlIniFile.close()


    #~ if SettingsFile.has_option('Commands', SentCommand):
        #~ CommandFromSettings = SettingsFile.get('Commands', SentCommand)
    #~ else:
        #~ CommandFromSettings = ''

    #~ if True:
    if 'samurai' in SentCommands:
        SentCommand = CreateCommandSamurai(SentCommands[0], SentCommands[1])
    #~ elif False:
    elif 'arvin' in SentCommands:
        SentCommand = CreateCommandAvrin(SentCommands[0], SentCommands[1])

    #~ if True:
    if 'samurai' in SentCommands:
        BinStr = "".join('%s' % bin(int(SentCommand[i: i + 2], 16))[2:].zfill(8)[::-1] for i in xrange(0, 7, 2))
        # start sequence + NEC start + bits + end pulse with long pause + end sequence
        EncodedCommand = '26002e01' + '00012b96' + "".join(('1440' if c == '1' else '1414') for c in BinStr) + '1400072a' + '000d05'
        RM3Device.send_data(EncodedCommand.decode('hex'))
    #~ elif False:
    elif 'arvin' in SentCommands:
        BinStr = "".join('%s' % bin(int(SentCommand[i: i + 2], 16))[2:].zfill(8)[::-1] for i in xrange(0, 11, 2))
        # start sequence + Samsung start + data + end pulse + (here the copy)Samsung start + data + end pulse with long pause + end sequence
        EncodedBinStr = "".join(('1434' if (c == '1') else '1414') for c in BinStr)
        EncodedCommand = '2600ca00' + '9494' + EncodedBinStr + '1494' + '9494' + EncodedBinStr + '1400072a' + '000d05'
        RM3Device.send_data(EncodedCommand.decode('hex'))
