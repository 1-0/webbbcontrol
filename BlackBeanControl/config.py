#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from flask import Flask


class ConfigClass(object):
    # Flask settings
    #~ SECRET_KEY =              os.getenv('SECRET_KEY',       'THIS IS AN INSECURE SECRET')
    #~ SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL',     'sqlite:///basic_app.sqlite')
    CSRF_ENABLED = True

    # Flask-Mail settings
    #~ MAIL_USERNAME =           os.getenv('MAIL_USERNAME',        'email@example.com')
    #~ MAIL_PASSWORD =           os.getenv('MAIL_PASSWORD',        'password')
    #~ MAIL_DEFAULT_SENDER =     os.getenv('MAIL_DEFAULT_SENDER',  '"MyApp" <noreply@example.com>')
    #~ MAIL_SERVER =             os.getenv('MAIL_SERVER',          'smtp.gmail.com')
    #~ MAIL_PORT =           int(os.getenv('MAIL_PORT',            '465'))
    #~ MAIL_USE_SSL =        int(os.getenv('MAIL_USE_SSL',         True))

    # Flask-User settings
    #~ USER_APP_NAME        = "AppName"                # Used by email templates

    # Statement for enabling the development environment
    DEBUG = True

    # Define the application directory
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))

    # Define the database - we are working with
    # SQLite for this example
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
    DATABASE_CONNECT_OPTIONS = {}

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED     = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "10101001111001195544;lih b0y789"

    # Secret key for signing cookies
    SECRET_KEY = '101001111001195544;lih b0y789'


def create_app():
    """ Flask application factory """

    # Setup Flask app and app.config
    app = Flask(__name__)
    app.config.from_object(__name__+'.ConfigClass')

    # Initialize Flask extensions
    #~ db = SQLAlchemy(app)                            # Initialize Flask-SQLAlchemy
    #~ mail = Mail(app)                                # Initialize Flask-Mail

    return app

