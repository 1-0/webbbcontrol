#!/usr/bin/env bash

git pull
docker stop webcc_c
docker rm webcc_c
docker rmi webcc:webcc
docker build -t webcc:webcc  .
docker run --name=webcc_c  -p 5551:5551 --restart=always -d webcc:webcc

