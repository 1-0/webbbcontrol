FROM python:2

WORKDIR /usr/src/app
#EXPOSE 5551:5551

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY telegram-send.conf ~/.config/
COPY . .
CMD [ "python", "./runserver.py" ]
