#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  runserver.py
#
#  Copyright 2014 a10 <a1_0 at i.ua>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from flask import Flask
#~ app = Flask(__name__)

from BlackBeanControl.views import *

if __name__ == '__main__':
    # ~ app.run(host='0.0.0.0') # host=None, port=None, debug=None, **options
    # ~ app.run(host='0.0.0.0', port = 5551, debug=True, threaded=True)
    app.run(host='0.0.0.0', port = 5551, threaded=True)

